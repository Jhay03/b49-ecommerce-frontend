import React, {useEffect, useState} from 'react';
import Swal from 'sweetalert2';
import { URL } from "../../config"

const EditProduct = ({setShowEdit, product}) => {
    const [formData, setFormData] = useState(product)
    const [categories, setCategories] = useState([])
    useEffect(() => {
        fetch(`${URL}/categories`, {
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                setCategories(data)
        })
    }, [product])

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleFile = (e) => {
        setFormData({
            ...formData,
            image: e.target.files[0]
        })
    }
    
    const onSubmit = (e) => {
        e.preventDefault()
        let updatedProduct = new FormData()
      
        updatedProduct.append('name', formData.name)
        updatedProduct.append('description', formData.description)
        updatedProduct.append('categoryId', formData.categoryId)
        updatedProduct.append('price', formData.price)
        if (typeof formData.image === 'object') {
            updatedProduct.append('image', formData.image)
        }
        fetch(`${URL}/products/${formData._id}`, {
            method: "PUT",
            body: updatedProduct,
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: "success",
                        'text': data.message
                    })
                    setShowEdit(false)
                } 
        })
        
    }
    return (
        <form className="mx-auto col-sm-6" onSubmit={onSubmit} encType="multipart/form-data">
            <h1 className="text-center" >Edit Details </h1>
            
                <div className="form-group">
                    <label htmlFor="name"> Name </label>
                    <input type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.name}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="description"> Description </label>
                    <input type="text"
                        name="description"
                        id="description"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.description}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="price"> Price </label>
                    <input type="text"
                        name="price"
                        id="price"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.price}
                        
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="image"> Image </label>
                <img src={`${URL}/${product.image}`} className="img-fluid"/>
                    <input type="file"
                        name="image"
                        id="image"
                        className="form-control"
                        onChange={handleFile}
                    />
            </div>
                  <select className="form-control" name="categoryId" onChange={onChangeHandler}>
                {categories.map(category => {
                    return (
                        category._id === formData.categoryId ? 
                            <option value={category._id} selected>{category.name}</option> :
                            <option value={category._id} key={category._id}>{category.name}</option>
                            )
                        })}
            </select>
            <button className="btn btn-outline-primary mt-2 mr-2" >Update</button>
            <button className="btn btn-outline-secondary mt-2" type="button" onClick={() => setShowEdit(false)}>Cancel</button>
            
           
        </form>
    )
}

export default EditProduct;