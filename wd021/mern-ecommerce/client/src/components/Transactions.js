import React, {useState, useEffect} from 'react';
import { URL } from "../config"

const Transactions = () => {
    const [transactions, setTransactions] = useState([])
    useEffect(() => {
        fetch(`${URL}/transactions`, {
            headers: {
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
        .then(data => setTransactions(data))
    })

    const updateStatus = (statusId, transactionId) => {
        let body = {
            statusId
        }
        fetch(`${URL}/transactions/${transactionId}`, {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
            alert(data.message)
        })
    }

 
 
    return (
        <div className="container">
            <h2 className="text-center"> Transaction History</h2>
            {
                transactions.length ?
                <table className="table table-hover">
                    <thead className="thead-dark">
                            <tr>
                                <th>Transaction ID</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>Action(s)</th>
                            </tr>   
                    </thead>
                    <tbody>
                            {transactions.map(transaction => (
                            <tr key={transaction._id}>
                                <td>{transaction._id}</td>
                                    <td>
                                    {transaction.statusId == "5e93c94dd1cfb0544d638c3e" ?
									"Pending" : (transaction.statusId == "5e93c93dd1cfb0544d638c2f" ?
									"Completed" : "Cancelled"
									)
								}
                                    </td>
                                    <td>PHP : {transaction.total}</td>
                                    {transaction.statusId === "5e93c94dd1cfb0544d638c3e" ? 
                                <td>
                                        <button className="btn btn-outline-primary mr-2"
                                                onClick={() => updateStatus("5e93c93dd1cfb0544d638c2f", transaction._id)}
                                                 >
                                             Approve
                                        </button>
                                        
                                        <button className="btn btn-outline-danger mr-2"
                                                onClick={() => updateStatus("5e93c955d1cfb0544d638c41", transaction._id)}
                                                 >
                                         Decline
                                        </button>
                                        
                                        </td> : null
                                }    
                            </tr>
                        ))}        
                    </tbody>
                </table>    
                : <h2 className="text-center"> No transactions to show. </h2>
            }
        </div>
    )
}

export default Transactions;