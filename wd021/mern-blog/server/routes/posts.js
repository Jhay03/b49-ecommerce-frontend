const express = require("express")
const router = express.Router()
const Post = require("../models/Post")
const jwt = require("jsonwebtoken")

//add a post localhost:4000/posts
router.post("/", (req, res) => {
    let token = req.headers["x-auth-token"]
    if (!token) return res.status(401).json({ message: "You are not logged in" })
    
    let decoded = jwt.verify(token, 'b49-blog')
    if (decoded) {
        const post = new Post()
        post.title = req.body.title
        post.description = req.body.description
        post.author = decoded.username
        post.save()
        return res.json(post)
    }
})
//get all post by id
//localhost:4000/posts/all
router.get("/all", (req, res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "Unauthorized!" })
    let decoded = jwt.verify(token, 'b49-blog')
    Post.find({ author: decoded.username }, (err, posts) => {
        return res.status(200).json(posts)
    })
})
//view all post localhost:4000/posts
router.get("/", (req, res) => {
    Post.find({}, (err, posts) => {
        return res.json(posts)
    })
})

//view single post localhost:4000/id
router.get("/:id", (req, res) => {
    Post.findOne({_id: req.params.id}, (err, post) => {
        return res.json(post)
    })
})

//edit single post localhost:4000/posts/id
router.put("/:id", (req, res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "Unauthorized" })
    
    let decoded = jwt.verify(token, 'b49-blog')
    if (decoded) {
        Post.findOne({ _id: req.params.id }, (err, post) => {
            if (post.author == decoded.username) {
                post.title = req.body.title
                post.description = req.body.description
                post.save()
                return res.status(200).json(post)
            } else {
                return res.status(401).json({message: "Unauthorized"})
            }
        })
    }
})

//delete a post localhost:4000/id
router.delete("/:id", (req, res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "Unauthorized!" })
    
    let decoded = jwt.verify(token, 'b49-blog')
    if (decoded) {
        Post.findOne({_id: req.params.id}, (err, post) => {
            if (post.author == decoded.username) {
                post.delete()
                return res.status(200).json({message: "Post Successfully Deleted!"})
            } else {
                return res.status(401).json({message: "Unauthorized"})
            }
        })
    }
})



module.exports = router;