const express = require("express")
const app = express()
const PORT = 4000
const cors = require("cors")
//db connection
const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost/b49_blog", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection
db.once('open', () => console.log("You are now connected to MongoDB"))

app.use(cors())
app.use(express.json())

//define routes
app.use("/users", require("./routes/users"))
app.use("/posts", require("./routes/posts"))

app.listen(PORT,() => console.log(`Server is running in port:${PORT}`))