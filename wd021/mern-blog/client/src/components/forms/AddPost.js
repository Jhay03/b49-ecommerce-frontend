import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'



const AddPost = ({token}) => {
    const [formData, setFormData] = useState({})
    const [result, setResult] = useState(false)
    const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
    }
//    console.log(formData)
    const onSubmitHandler = () => {
        //alert("Added A post")
        fetch("http://localhost:4000/posts", {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": token
            }
        })
            .then(res => res.json())
            .then(data => {
                setResult(true)
            })
      
    }
    return (
        <div className="container col-sm-6 mx-auto">
            <h2>Add Post</h2>
            <form>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                className="form-control"
                type="text"
                name="title"
                id="title"
                onChange={onChangeHandler}
                />
            </div>   
            <div className="form-group">
                <label htmlFor="description">Description</label>
                <input
                className="form-control"
                type="text"
                name="description"
                id="description"
                onChange={onChangeHandler}
                />
            </div>       
                 
            </form>
            <button className="btn btn-outline-secondary"
                onClick={() => onSubmitHandler()}> Add Post </button>
            { result ? <Redirect to="/" />:null}
        </div>
    )
}

export default AddPost;