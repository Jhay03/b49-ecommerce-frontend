import React, {useState} from 'react';

const Login = () => {
    const [formData, setFormData] = useState({})
    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const onSubmitHandler = (e) => {
        e.preventDefault()
        fetch("http://localhost:4000/users/login", {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
           // console.log(data)
                localStorage.setItem("user", JSON.stringify(data.user))
                localStorage.setItem("token", data.token)
                window.location.href="/"
        })
        // alert("Submit")
    }
    return (
            <div className="container col-sm-6">
            <h2 className="my-3">Login</h2>
            <form onSubmit={onSubmitHandler}>
                <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input
                        type="text"
                        className="form-control"
                        id="username"
                        name="username"
                        onChange={onChangeHandler}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        className="form-control"    
                        id="password"
                        name="password"
                        onChange={onChangeHandler}
                    />
                </div>
                <button className="btn btn-outline-primary">Login</button>
            </form>
            </div>
    )
}

export default Login;