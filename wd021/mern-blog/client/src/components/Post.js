import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import EditPost from './forms/EditPost';

const Post = ({ user, token }) => {
    let { id } = useParams()
    const [post, setPost] = useState({})
    const [showEdit, setShowEdit] = useState(false)
    useEffect( () => {
        fetch(`http://localhost:4000/posts/${id}`)
            .then(res => res.json())
            .then(data => {
                setPost(data)
            })
    }, [post])
      
    const deleteHandler = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
                  fetch(`http://localhost:4000/posts/${id}`, {
                      method: "DELETE",
                      headers: {
                          "x-auth-token": token
                      }
                  })
                      .then(res => res.json())
                      .then(data => {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                          )
                          window.location.href="/"
                  })
               }
          })
    }

   
   
    return (
        <div className="container" key={post.id}>
            <h2> Post Details</h2>
            <div className="row">
                <div className="col col-lg-4">
                    <div className="card mb-2">
                        {showEdit ? <EditPost post={post} setShowEdit={setShowEdit} token={token}/> :
                        <div className="card-body">
                        <h3 className="text-center">{post.title}</h3>
                        <p>Description : {post.description}</p>
                        <h6>Author : {post.author}</h6>
                        <div className="card-footer">
                            {user && post.author === user.username && token ?
                                <div>
                                    <button className="btn btn-outline-warning mr-2" onClick={() => setShowEdit(true)}>Edit</button>
                                    <button className="btn btn-outline-danger" onClick={deleteHandler}>Delete</button>
                                </div>
                            :null}  
                        </div>
                        </div>}
                       
                    </div>
                </div>
            </div>
            
            
        </div>
    )
}

export default Post;