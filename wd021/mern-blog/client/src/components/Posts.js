import React from 'react';
import { Link } from 'react-router-dom';

const Posts = ({ posts }) => {
    //console.log(posts)
    let allPosts = posts.map(post => (
        <div className="container" key={post._id}>
            <div className="row">
                <div className="col col-lg-4">
                    <div className="card mb-3" key={post.id}>
                        <div className="card-body">
                            <h4 className="card-title">{post.title}</h4>
                            <p className="card-text">{post.description}</p>
                            <small>{post.author}</small>
                            <br />
                            <Link to={`/posts/${post._id}`}> View Post Details</Link>
                        </div>
                    </div>    
                </div>
            </div>
       </div>
    ))
    return (
        <div className="container">
            <h2 className="my-3"> All post</h2>
            {allPosts}
        </div>
    )
}
export default Posts;