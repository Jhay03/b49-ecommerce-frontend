import React from 'react';
function Header() {
    return (
        <header style={headerStyle}>
            <h1>Todo List</h1>
        </header>
    )
}

const headerStyle = {
    backgroundColor: 'gray',
    color: 'white',
    textAlign: 'center',
    padding: '1px'
}

export default Header;