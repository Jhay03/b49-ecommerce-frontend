import React, {useState} from 'react';

const EditTodo = ({ setEditing, id, title, editTodo }) => {
    const [formData, setFormData] = useState({
        id,
        title
    })

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    return (
        <div>
            <h2>Editing Todo</h2>
            <label htmlFor="title">Title</label>
            <input
                name="title"
                type="text"
                value={formData.title}
                onChange={onChangeHandler}
            />
            <br/>
            <label> Is completed: </label>
            <input type="checkbox" name="status" />
            <br/>
            <button onClick={() => {
                editTodo(formData)
                setEditing(false)
            } }> Submit</button>
            <button onClick={() => setEditing(false)}> Cancel </button>
        </div>
    )
}


export default EditTodo;