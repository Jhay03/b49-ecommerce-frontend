import React, { useState } from 'react';

function App() {
  const [show, setShow] = useState(false)
 return (
   <div className="App">
     {show ?
       <h2>This will only show if its true</h2> : null}
     <button onClick={() => setShow(!show)}>
       {show ? "Hide Content " : " Show Content"}
    
       </button>
    </div>
  );
}

export default App;
