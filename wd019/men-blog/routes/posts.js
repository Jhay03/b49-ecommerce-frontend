const express = require("express");

const router = express.Router(); // storing Router function to router variable.

const Post = require("../models/Post.js");

const jwt = require("jsonwebtoken")

const User = require("../models/User.js")

//add a post 
router.post("/", (req, res) => {
    //console.log(req.body)

    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "You are not logged in." })
    
    let decoded = jwt.verify(token, 'b49-blog')

    if (decoded) {
        const post = new Post()
        post.title = req.body.title;
        post.description = req.body.description;
        post.author = decoded.username;
        post.save();
        return res.json(post)
    }
   
})

//view all 

router.get("/", (req, res) => {
    Post.find({}, (err, posts) => { 
        return res.json(posts)
    })
})

//view single post by id
router.get("/:id", (req, res) => {
    Post.findOne({ _id: req.params.id }, (err, post) => {
        return res.json(post)
    })
 })
//edit a post
router.put("/:id", (req, res) => {

    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "Unauthorized" })
    
    let decoded = jwt.verify(token, 'b49-blog')

    if (decoded) {
        Post.findOne({ _id: req.params.id }, (err, post) => {
            if (post.author == decoded.username) {
                post.title = req.body.title
                post.description = req.body.description
                post.save((err) => {
                    if (err) {
                        return res.status(500).json({message: "Server Error"})
                    } else {
                        return res.status(200).json(post)
                    }
                })
            } else {
                return res.status(401).json({message: "You are not the owner of the post    "})
            }
        })
    }
   
    
})
//delete a post
router.delete("/:id", (req, res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "Unauthorized" })
    
    let decoded = jwt.verify(token, 'b49-blog')
    if (decoded) {
        User.findOne({ username: decoded.username }, (err, user) => {
            if (user.username == decoded.username) {
                Post.findOneAndDelete({ _id: req.params.id }, (err, post) => {
                    console.log("Successfully Deleted")
                    return res.json(post)
                })
            }
        })
    }

})


module.exports = router;